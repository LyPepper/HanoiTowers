package com.mycompany.torres;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javax.swing.JOptionPane;

public class FXMLController implements Initializable {

    @FXML
    private TextField txtcaja;

    @FXML
    private Label lblText;

    @FXML
    private void accionmostrar(ActionEvent event) {
        System.out.println("Show Solution");

        int num = Integer.parseInt(txtcaja.getText());
        System.out.println(txtcaja.getText());

        if (num > 10 || num <= 0) {

            lblText.setText("Rango no permitido");
            //JOptionPane.showMessageDialog(null, "Rango no permitido");
        } else {
             lblText.setText("");
            TorresFrame tf = new TorresFrame(Integer.parseInt(txtcaja.getText()));
            tf.setVisible(true);
            tf.setBounds(0, 0, 510, 550);
            tf.setLocationRelativeTo(null);
            tf.setTitle("Solución");

        }
    }

    @FXML
    private void accionregresar(ActionEvent event) {
        System.exit(0);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }
}
