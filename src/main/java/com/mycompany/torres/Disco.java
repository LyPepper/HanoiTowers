package com.mycompany.torres;

/**
 *
 * @author CinthyaLiliana
 */
import java.awt.*;
import java.util.Random;

public class Disco {

    private int base, altura;
    Random rand = new Random();
    float r = rand.nextFloat();
    float g = rand.nextFloat();
    float bo = rand.nextFloat();
    Color randomColor = new Color(r, g, bo);

    public Disco(int b, int h) {

        this.base = b;
        this.altura = h;
    }

    public void dibuja(Graphics g, int x, int y) {

        g.setColor(randomColor);
        g.fillRoundRect(x - base / 2, y - altura, base, altura, 10, 10);
        g.setColor(randomColor);
        g.drawRoundRect(x - base / 2, y - altura, base, altura, 10, 10);
    }
}
