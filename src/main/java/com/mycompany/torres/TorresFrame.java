/*
 frame.setLayout(new floLayout(FlowLayout.CENTER));
frame.setLayout(new floLayout(FlowLayout.RIGHT));
frame.setLayout(new floLayout(FlowLayout.LEFT));
El FlowLayout, es aquel layout q ubica a todos los componentes en forma horizontal, en el orden q le digamos.
 */
package com.mycompany.torres;

/**
 *
 * @author CinthyaLiliana
 */
import java.awt.*;

public class TorresFrame extends Frame {

    private Hanoi h;
    private boolean resolver;

    public TorresFrame(int ndiscos) {

        setLayout(new FlowLayout());
        h = new Hanoi(ndiscos);
        resolver = true;
    }

    public void paint(Graphics g) {

        h.dibuja(g);
        if (resolver) {
            h.resuelve(g);
        }
    }

}
